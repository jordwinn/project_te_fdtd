

function [V]=FTDTalgorithm(Rc,v,lengthincm,Zg,Zl,bitrate)
%berekenen van de delta z en delta t en tau, rekening houdend met de 
%courrant limiet
length=0.01*lengthincm;
lambda=v/bitrate;
deltaz=min(lambda/100,0.2);
aantaliteratiesz=ceil(length/deltaz);
deltazmatrix=zeros(1,aantaliteratiesz);
deltamax=deltaz/v;
deltat=deltamax/2;

for j=1:aantaliteratiesz
    deltazmatrix(j)=j*deltaz;
end;

%We berekenen L en C om te gebuiken in de update functies
C=1/(v*Rc);
L=Rc/v;
ct1=deltat/(C*deltaz);
ct2=deltat/(L*deltaz);

%Stel we willen de simulatie laten lopen zolang zodat de 1ste bit 20 keer
%de lijn is afgelopen
tmax=4*length/v;
aantaliteratiest=ceil(tmax/deltat);
deltatmatrix=zeros(1,aantaliteratiest);

%We willen een functie definieren voor Vg die overeenkomt met de bitrate
%We nemen een functie die gedurende ongeveer 200 deltat's stijgt, dan
%gedurende de periode hoog blijft, dan voor 200 deltat' daalt en voor de 
%rest laag blijft.
periode=1/bitrate;
Vg=zeros(1,aantaliteratiest); %op gehele + 1/2 tijdstippen (bronspanning)
for i=1:aantaliteratiest
    t=i*deltat;
    if(0<=t && t<=periode/10)
        Vg(i)=(10/periode)*t;
    elseif(periode/10<t && t<9*periode/10)
        Vg(i)=1;
    elseif(9*periode/10<=t && t<=periode)
        Vg(i)=(-10/periode)*t+10;
    else
        Vg(i)=0;
    end;
end;

V=zeros([aantaliteratiest,aantaliteratiesz]); %op gehele tijdstippen en plaatsen
I=zeros([aantaliteratiest,aantaliteratiesz-1]); %op gehele + 1/2 tijdstippen en plaatsen


%We willen ook een real time 'animatie' van de spanningen over de lijn
m=1;
Vplot=plot(deltazmatrix,V(m,:));
axis([0 length -1/2 3/2]);
title('Simulatie van de spanningsbit');
xlabel('Plaats op de lijn');
ylabel('Spanning');

for m=1:aantaliteratiest
    for k=1:aantaliteratiesz
        %We beginnen met de boundary condition aan de linker kant voor k=0
        %Daarvoor komen we in de problemen voor de spanning V0
        
        %Eerst en vooral stellen we de grensconditie op voor het geval dat
        %de simulatie net begint.
        if(k==1 && m==1)
            I(1,1)=0;
            V(2,1)=((2*Vg(1))/Zg)*(1/(1/(ct1*2)+1/(2*Zg)));
        
        %Dan de grensconditie voor k=0 en m niet gelijk aan nul, dus verder
        %in de simulatie.
        elseif(k==1 && m~=1)
            if(m~=aantaliteratiest)
                I(m,1)=I(m-1,1)-ct2*(V(m,2)-V(m,1));
                V(m+1,1)=((2*Vg(m)-V(m,1))/(2*Zg)-I(m,1)+V(m,1)/(ct1*2))*(1/(1/(ct1*2)+1/(2*Zg)));
            else
                %Voor de laatste iteratie moeten we V niet meer berekenen
                %want die ligt dan uit het bereik van de matrix
                I(m,1)=I(m-1,1)-ct2*(V(m,2)-V(m,1));
            end;
        
        %Voor de rest van de lijn indien we net met de simulatie starten
        elseif(m==1 && k~=1)
            I(1,k)=0;
            V(2,k)=0;
        
        %Eind voorwaarden op k=max voor I want bij V is er geen probleem
        elseif(k==aantaliteratiesz && m~=1)
            if(m~=aantaliteratiest)
                V(m+1,aantaliteratiesz)=V(m,aantaliteratiesz)*((1-deltat/(C*deltaz*Zl))/(1+(deltat/(C*deltaz*Zl))))+2*deltat*Zl*I(m,aantaliteratiesz-1)/((C*deltaz)*(Zl+(deltat/(C*deltaz))));
            end;
            %Voor alle andere gevallen gelden de algemene formules
        else
            if(m~=aantaliteratiest)
                I(m,k)=I(m-1,k)-ct2*(V(m,k+1)-V(m,k));
                V(m+1,k)=V(m,k)-ct1*(I(m,k)-I(m,k-1));
            else
                I(m,k)=I(m-1,k)-ct2*(V(m,k+1)-V(m,k));
            end;
        end;
    end;
    deltatmatrix(m)=m*deltat;
    
    %Deel van de animatie
    if(mod(m,25)==0)
        set(Vplot,'Ydata',V(m,:));
        refreshdata;
        drawnow
        axis([0 length -1/2 3/2]);
        title('Simulatie van de spanningsbit');
        xlabel('Plaats op de lijn');
        ylabel('Spanning');
    end;
    
end;

%Eerst willen we eens plotten wat er gebeurt over de hele lijn na 1/2 en na
%de volledige simulatie.
t12=floor(aantaliteratiest/2);
figure
ax1=subplot(2,1,1);
ax2=subplot(2,1,2);

plot(ax1,deltazmatrix,V(t12,:),'g');
title(ax1,'Spanning na de helft van de simultie');
xlabel(ax1,'lengte van de lijn');

plot(ax2,deltazmatrix,V(aantaliteratiest,:),'g');
title(ax2,'Spanning na de volledige simulatie');
xlabel(ax2,'lengte van de lijn');

figure
bx1=subplot(2,1,1);
bx2=subplot(2,1,2);

plot(bx1,deltazmatrix,I(t12,:),'y');
title(bx1,'Stroom na de helft van de simultie');
xlabel(bx1,'lengte van de lijn');

plot(bx2,deltazmatrix,I(aantaliteratiest,:),'y');
title(bx2,'Stroom na de volledige simulatie');
xlabel(bx2,'lengte van de lijn');

%Dan willen we eens plotten wat er gebeurt over de hele simulatie in het
%begin in het midden en op het einde van lijn.
%begin
x12=floor(aantaliteratiesz/2);
figure
at1=subplot(2,1,1);
at2=subplot(2,1,2);

plot(at1,deltatmatrix,transpose(V(:,1)),'g');
title(at1,'Spanning in het begin van de lijn gedurende de simulatie');
xlabel(at1,'tijd van de simulatie');

plot(at2,deltatmatrix,transpose(I(:,1)),'y');
title(at2,'Stroom in het begin van de lijn gedurende de simulatie');
xlabel(at2,'tijd van de simulatie');

%midden
figure
bt1=subplot(2,1,1);
bt2=subplot(2,1,2);

plot(bt1,deltatmatrix,transpose(V(:,x12)),'g');
title(bt1,'Spanning in het midden van de lijn gedurende de simulatie');
xlabel(bt1,'tijd van de simulatie');

plot(bt2,deltatmatrix,transpose(I(:,x12)),'y');
title(bt2,'Stroom in het midden van de lijn gedurende de simulatie');
xlabel(bt2,'tijd van de simulatie');

%einde
figure
ct1=subplot(2,1,1);
ct2=subplot(2,1,2);

plot(ct1,deltatmatrix,transpose(V(:,aantaliteratiesz)),'g');
title(ct1,'Spanning in het einde van de lijn gedurende de simulatie');
xlabel(ct1,'tijd van de simulatie');

plot(ct2,deltatmatrix,transpose(I(:,aantaliteratiesz)),'y');
title(ct2,'Stroom in het einde van de lijn gedurende de simulatie');
xlabel(ct2,'tijd van de simulatie');

end