function [ V,I] = FDTD
%FDTD this function performs the FDTD algorithm.
%Following input will be asked:
%   Rc: characteristic impedance
%   v: signal speed in m/s
%   d: length of the line in cm
%   Rg: the real valued generator impedance
%   Rl: the real valued load impedance
%   b: the bitrate



%% Simple Dialog Feed

% Enter Zc
prompt = 'Enter the characteristic impedance. Press ''Enter'' to confirm. \n Rc = ' ;
Rc = input(prompt);

% Enter v
prompt = 'Enter the signal speed in x per seconds. Press ''Enter'' to confirm. \n v = ';
v = input(prompt);

% Enter d
prompt = 'Enter the length of the line in cm. Press ''Enter'' to confirm. \n d = ';
d = input(prompt);

% Enter Rg
prompt = 'Enter the real-valued generator impedance. Press ''Enter'' to confirm. \n Rg = ';
Rg = input(prompt);

% Enter Rl
prompt = 'Enter the real-valued load impedance. Press ''Enter'' to confirm. \n Rl = ';
Rl = input(prompt);

% Enter bitrate
prompt = 'Enter the bitrate. Press ''Enter'' to confirm. \n b = ';
b = input(prompt);

%% Defining needed symbols and vectors

dz=v/(b*100);
N=ceil(d/(100*dz));            %N is de maximale waarde voor de plaats, we doen *100 om cm/cm te krijgen
dzvector=zeros(1,N);
dt = dz/(2*v);                  %kleiner dan Courant limiet

 
tijd = 4*d/(100*v);            %de blokgolf heeft nu tijd om 4 keer de lijn af te leggen (2RTT's)
M=ceil(tijd/dt);                % M is de maximale waarde voor de tijd
dtvector=zeros(1,M);

L = Rc/v;
C = 1/(Rc*v);

V = zeros(M,N);
I = zeros(M,N);           

for n=1:N
    dzvector(n)=dz*n;           % dzvector voor plot
end;

for m=1:M
    dtvector(m)=dt*m;           % dtvector voor plot
end;

%% FDTD algorithm
figure
%grenzen van de plot
boundV=max(Rc/(Rg+Rc),max(Rc/(Rg+Rc)*(1+(Rl-Rc)/(Rl+Rc)),Rc/(Rg+Rc)*(Rl-Rc)/(Rl+Rc)*(1+(Rg-Rc)/(Rg+Rc))))*1.1;
boundI=max(1/(Rg+Rc),max(1/(Rg+Rc)*(1-(Rl-Rc)/(Rl+Rc)),1/(Rg+Rc)*(Rl-Rc)/(Rl+Rc)*(1-(Rg-Rc)/(Rg+Rc))))*1.1;

boven=subplot(2,1,1);
Vplot=plot(boven,dzvector,V(1,:));
axis([0 d/100 -boundV boundV]);
title('Simulatie van de spanningsbit');
xlabel('Plaats op de lijn');
ylabel('Spanning[V]');

onder= subplot(2,1,2);
Iplot=plot(onder,dzvector,I(1,:));
axis([0 d/100 -boundI boundI]);
title('Simulatie van de stroom door de Transmissielijn');
xlabel('Plaats op de lijn');
ylabel('Stroom[A]');



%simuleer een enkele eindig snelle blokpuls
BG=zeros(1,M);

for t=1:M
    tijd=dt*t;
    if ( 0 <= tijd && tijd<1/(10*b))
        BG(t)=b*10*tijd;                                 
    elseif(tijd >= 1/(10*b) && tijd < 9/(10*b))
        BG(t)=1;
    elseif(tijd >= 9/(10*b) && tijd < 1/b)
        BG(t)= 10-tijd*b*10;
    else
        BG(t)=0;
    end;
end;

for m = 1:M
    for n = 1:N
        % grensconditie in het begin van de lijn als de simulatie net begint
        if(n==1 && m==1)
            I(1,1)=0;                                            
            V(2,1)=2*BG(1)*dt/(C*Rg*dz+dt);      
       
        % initialisatie van de lijn bij begin van de simulatie
        elseif (n~=1 && m==1)      
              I(1,n) = 0;         
              V(2,n) = 0;
             
        % grensconditie in het begin van de lijn en verder in de simulatie        
        elseif (n==1 && m~=1)
            
            if(m~=M)
                I(m,1)=I(m-1,1)-dt/(L*dz)*(V(m,2)-V(m,1));
                V(m+1,1)=((2*BG(m)-V(m,1))/(2*Rg)-I(m,1)+V(m,1)*C*dz/(2*dt))*1/(1/(2*Rg)+(C*dz)/(2*dt));
            else
                I(m,1)=I(m-1,1)-dt/(L*dz)*(V(m,2)-V(m,1)); 
            end;
        
        %grensconditie op het einde van de lijn
        elseif(n==N && m~=1)
            if(m~=M)
                V(m+1,N)=V(m,N)*((1-dt/(C*dz*Rl))/(1+dt/(C*dz*Rl)))+(I(m,N-1)*2*dt/(C*dz))/(1+dt/(C*dz*Rl));
            end;
            
       %Voor alle andere gevallen gelden de algemene formules
        else
            if(m~=M)
                I(m,n)=I(m-1,n)-dt/(L*dz)*(V(m,n+1)-V(m,n));
                V(m+1,n)=V(m,n)-dt/(C*dz)*(I(m,n)-I(m,n-1));
            else
                I(m,n)=I(m-1,n)-dt/(L*dz)*(V(m,n+1)-V(m,n));
            end;
        end;      
    end;
    
    %Deel van de animatie
    if(mod(m,25)==0)
        set(Vplot,'Ydata',V(m,:));
        refreshdata;
        drawnow
       
        set(Iplot,'Ydata',I(m,:));
        refreshdata;
        drawnow
    end;
    
end;

%% Producing some figures for the report
tstart= round(M/32);		   %de +M/32 bij tstart is om juist de bit al wat in beeld te hebben
thalf = round(M/2);                % tijd na de helft van de simulatie
tkwart = round (M/4);
t3kwart = round (M*3/4);


figure

figtbegin = subplot(5,1,1);
plot(figtbegin,dzvector,V(tstart,:),'k');
title('Spanning bij het begin van de simulatie')

figtkwart = subplot (5,1,2);
plot(figtkwart,dzvector,V(tkwart,:),'k');
title('Spanning na een kwart van de simulatie')

figtmidden = subplot(5,1,3);
plot(figtmidden,dzvector,V(thalf,:),'k');
title('Spanning na helft van de simulatie')

figt3kwart = subplot (5,1,4);
plot(figt3kwart,dzvector,V(t3kwart,:),'k');
title('Spanning na driekwart van de simulatie')

figteind = subplot (5,1,5);
plot(figteind,dzvector,V(M,:),'k');
title('Spanning bij het einde van de simulatie')

ylabel('Spanning (V)');
xlabel('Plaats op de lijn (m)');

%analoog voor de stroom

figure
figtbeginb = subplot(5,1,1);
plot(figtbeginb,dzvector,I(tstart,:),'k');
title('Stroom bij het begin van de simulatie')

figtkwartb = subplot (5,1,2);
plot(figtkwartb,dzvector,I(tkwart,:),'k');
title('Stroom na een kwart van de simulatie')

figtmiddenb = subplot(5,1,3);
plot(figtmiddenb,dzvector,I(thalf,:),'k');
title('Stroom na helft van de simulatie')

figt3kwartb = subplot (5,1,4);
plot(figt3kwartb,dzvector,I(t3kwart,:),'k');
title('Stroom na driekwart van de simulatie')

figteindb = subplot (5,1,5);
plot(figteindb,dzvector,I(M,:),'k');
title('Stroom bij het einde van de simulatie')

ylabel('Stroom (A)');
xlabel('Plaats op de lijn (m)');

end
